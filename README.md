# Viz-mapping

A panel that allows you to associate numeric values with elements of a network in order to apply a specific style corresponding to those values.

## Props

### VizMapping

| Props | Type | default | Description | Optional |
| ----- | ---- | ------- | ----------- | -------- |
| draggable | boolean | false | Allows you to move the panel | Yes |
| inputAvailable | boolean | true | Allows you to display or not a load file input slot | Yes |
| mappings | {[key: string]: {[key: string]: string or Array<string>}} | {} | If you want to directly inject mapping data into the component, this property allows you to pass the mapping values associated with your objects' identifiers | Yes |
| x | number | 0 | X panel's position | Yes |
| y | number | 0 | Y panel's position | Yes |

## Types

### sendData

| Attribut | Type | Description |
| -------- | ---- | ----------- |
| style | string | Graph's style to apply on |
| targetLabel | string | Label use to find element in graph |
| type | string | Mapping's type (identified, continuous or discrete) |
| mappingName | string | Mapping's name |
| conditionName | string | Mapping condition name |
| data | {[key: string]: {[key: string]: string or number or Array<string>}} | Object that contains element id and mapping value |
| values | string or number or Function or {[key: string]: string or number} | Style value |

### removeData

| Attribut | Type | Description |
| -------- | ---- | ----------- |
| mappingName | string | Mapping's name |
| mappingCondition | string | Condition's name |
| type | string | Mapping's type (identified, continuous or discrete) |
| style | string | Style apply on |

## Events

### MappingWindow

| Name | Trigger | Output |
| ---- | ------- | ------ |
| launchMapping | Click on Launch button | sendData |
| removeMapping | Click on trash icon | removeData |

## Use the package in another project

```sh
npm i -D @metabohub/viz-mapping
```

If your project is not using vuetify, you need to import it in `src/main.ts` :
```ts
import { createApp } from 'vue'
import App from './App.vue'
import { vuetify } from "@metabohub/viz-mapping";

createApp(App).use(vuetify).mount('#app')
```

Use the component : 
```ts
<script setup lang="ts">
import { VizMapping } from "@metabohub/viz-mapping";

function applyMapping(mappingData) {
	const style = mappingData.style;
	const values = mappingData.values;
	const targetLabel = mappingData.targetLabel;
	const mappingName = mappingData.mappingName;
	const data = { ...mappingData.data };
	const type = mappingData.type;
	const conditionName = mappingData.conditionName;

	...
}

function removeMapping(mappingToRemove) {
		const mappingName = mappingToRemove.mappingName;
		const conditionName = mappingToRemove.mappingCondition;
		const mappingType = mappingToRemove.type;
		const style = mappingToRemove.style;

    ...
}
</script>

<template>
  <VizMapping
    :draggable="true"
    @launchMapping="applyMapping"
    @removeMapping="removeMapping"
  />
</template>

<style>
import "@metabohub/viz-mapping/dist/style.css";
</style>
```