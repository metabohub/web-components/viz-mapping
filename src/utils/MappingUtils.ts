/**
 * Takes the data from the input file and processes it to return formatted data
 * @param rawData Data from input file 
 * @returns {Object} Process data 
 */
export function processData(rawData: string) {
  const data: {[key: string]: any} = {
    id: [],
    targetLabel: ''
  };

  const lines = rawData.split('\n');
  const header = lines[0];
  const headerSplit = header.split('\t');
  data.targetLabel = headerSplit[0];

  // Only ID map
  if (headerSplit.length === 1) {
    for (let i = 1; i < lines.length; i++) {
      if (lines[i] !== "") {
        data["id"].push(lines[i]);
      }
    }
  }

  // ID + data map
  if (headerSplit.length > 1) {
    const condList = [];
    for (let j = 1; j < headerSplit.length; j++) {
      condList.push(headerSplit[j]);
      data[headerSplit[j]] = {};
    }
    for (let i = 1; i < lines.length; i++) {
      if (lines[i] !== "") {
        const line = lines[i].split('\t');
        data["id"].push(line[0]);
        for (let g = 0; g < condList.length; g++) {
          data[condList[g]][line[0]] = line[g+1];
        }
      }
    }
  }

  return data;
}