import { describe, it, expect } from 'vitest';
import { processData } from '../MappingUtils';

describe('processData', () => {

  it('should correctly process ID-only data', () => {
    const rawData = `ID\n123\n456\n789\n`;
    const expectedData = {
      id: ['123', '456', '789'],
      targetLabel: 'ID'
    };

    const result = processData(rawData);
    expect(result).toEqual(expectedData);
  });

  it('should correctly process data with ID and additional columns', () => {
    const rawData = `ID\tValue1\tValue2\n123\t10\t20\n456\t30\t40\n789\t50\t60\n`;
    const expectedData = {
      id: ['123', '456', '789'],
      targetLabel: 'ID',
      Value1: {
        '123': '10',
        '456': '30',
        '789': '50'
      },
      Value2: {
        '123': '20',
        '456': '40',
        '789': '60'
      }
    };

    const result = processData(rawData);
    expect(result).toEqual(expectedData);
  });

  it('should handle empty lines correctly', () => {
    const rawData = `ID\n123\n\n456\n789\n`;
    const expectedData = {
      id: ['123', '456', '789'],
      targetLabel: 'ID'
    };

    const result = processData(rawData);
    expect(result).toEqual(expectedData);
  });

  it('should handle an empty input string', () => {
    const rawData = '';
    const expectedData = {
      id: [],
      targetLabel: ''
    };

    const result = processData(rawData);
    expect(result).toEqual(expectedData);
  });

  it('should handle data with multiple tabs correctly', () => {
    const rawData = `ID\tValue1\tValue2\n123\t\t20\n456\t30\t\n789\t50\t60\n`;
    const expectedData = {
      id: ['123', '456', '789'],
      targetLabel: 'ID',
      Value1: {
        '123': '',
        '456': '30',
        '789': '50'
      },
      Value2: {
        '123': '20',
        '456': '',
        '789': '60'
      }
    };

    const result = processData(rawData);
    expect(result).toEqual(expectedData);
  });

});