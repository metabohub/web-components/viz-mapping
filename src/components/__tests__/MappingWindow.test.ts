import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import MappingWindow from '../MappingWindow.vue';
import vuetify from '../../plugins/vuetify';

global.ResizeObserver = require('resize-observer-polyfill');

const mappings = {
  CA1: {
    conditions: ['cond1'],
    data: "nodeIdentifier\tcond1\nA\t12\nR1\t24\nB\t48\nR2\t-1\nC\t42"
  },
  CA2: {
    conditions: ['cond1', 'cond2'],
    data: "nodeIdentifier\tcond1\tcond2\nA\t12\t10\nR1\t24\t30\nB\t48\t55\nR2\t-1\t-2\nC\t42\t67"
  }
}

describe('Test for MappingWindow component', () => {
  it('Should import and mount component without props', () => {
		expect(MappingWindow).toBeTruthy();

    const wrapper = mount(MappingWindow, {
			global: {
				plugins: [vuetify]
			}
		});

		expect(wrapper.html()).toBeTruthy();
  });

	it('Should import and mount component with props', () => {
		expect(MappingWindow).toBeTruthy();

    const wrapper = mount(MappingWindow, {
			global: {
				plugins: [vuetify]
			},
			props: {
				draggable: false,
        inputAvailable: false,
        mappings: mappings
			}
		});

		expect(wrapper.html()).toBeTruthy();
  });
});