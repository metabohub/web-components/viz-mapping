export interface sendData {
  style: string;
  targetLabel: string;
  values: string | number | Function | {[key: string]: string | number};
  mappingName: string;
  data: {[key: string]: {[key: string]: string | number | Array<string>}};
  type: string;
  conditionName: string;
}