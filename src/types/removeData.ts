export interface removeData {
  mappingName: string;
  mappingCondition: string;
  type: string;
  style: string;
}